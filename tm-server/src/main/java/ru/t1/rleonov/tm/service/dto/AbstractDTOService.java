package ru.t1.rleonov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.rleonov.tm.api.repository.dto.IDTORepository;
import ru.t1.rleonov.tm.api.service.IConnectionService;
import ru.t1.rleonov.tm.api.service.dto.IDTOService;
import ru.t1.rleonov.tm.dto.model.AbstractModelDTO;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.Collection;

public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>>
        implements IDTOService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract R getRepository(@NotNull final EntityManager entityManager);

    @Override
    public void add(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IDTORepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.add(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void set(@NotNull final Collection<M> models) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IDTORepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.set(models);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IDTORepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.update(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@NotNull final M model) {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IDTORepository<M> repository = getRepository(entityManager);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            repository.remove(model);
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
