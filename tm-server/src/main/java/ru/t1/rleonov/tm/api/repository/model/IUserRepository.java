package ru.t1.rleonov.tm.api.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.rleonov.tm.model.User;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    List<User> findAll();

    @Nullable
    @SneakyThrows
    User findOneById(@Nullable String id);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    Boolean isLoginExist(@Nullable String login);

    Boolean isEmailExist(@Nullable String email);

    void clear();

}
